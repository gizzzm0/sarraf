# Sarraf

A mobile application for the Sarraf inter-denominational and cross-platform money transfer solution

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What you need to run the code and how to install them.

1. NodeJS : https://nodejs.org/en/download/
2. Ionic Framework : https://ionicframework.com/getting-started#cli
3. PouchDB: https://pouchdb.com/

### Installing

## install dependencies
`npm install`
    or
`yarn install`

## install dependencies
`ionic serve`

### Running tests

TODO: Unit and e2e tests

## Deployment
https://ionicframework.com/docs/intro/deploying/

## Built With

Ionic Framework

## Contributing



## Versioning

I use [SemVer](http://semver.org/) for versioning. https://github.com/your/project/tags 

## Authors

* **Gideon Muiru** 
