import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule } from '@angular/common/http';
import { JwtModule, JWT_OPTIONS } from '@auth0/angular-jwt';
import { Storage } from '@ionic/storage';

//Ionic Native
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { Base64 } from '@ionic-native/base64';
import { OneSignal } from '@ionic-native/onesignal';

//Third Party Modules
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { DatePickerModule } from 'ionic3-datepicker';

//Pages
import { MyApp } from './app.component';
import { SplashPage } from '../pages/splash/splash';

//Services
//---Miscellaneous
import { ValidatorProvider } from '../providers/validator/validator';
import { SmsServiceProvider } from '../providers/sms-service/sms-service';
//---Database
import { AccountsDbServiceProvider } from '../providers/accounts-db-service/accounts-db-service';
import { BankDbServiceProvider } from '../providers/bank-db-service/bank-db-service';
import { TelcoDbServiceProvider } from '../providers/telco-db-service/telco-db-service';
//---WebAPI
import { UserNetServiceProvider } from '../providers/user-net-service/user-net-service';
import { AccountsNetServiceProvider } from '../providers/accounts-net-service/accounts-net-service';
import { BankNetServiceProvider } from '../providers/bank-net-service/bank-net-service';
import { TelcoNetServiceProvider } from '../providers/telco-net-service/telco-net-service';
import { TransactionsNetServiceProvider } from '../providers/transactions-net-service/transactions-net-service';
import { PushNotificationProvider } from '../providers/push-notification/push-notification';
//Tests


//Getting jwt access token
const storage = new Storage({ driverOrder: ['sqlite', 'indexeddb', 'websql', 'localstorage'] });
export function jwtOptionsFactory() {
  return {
    tokenGetter: () => {
      return storage.get('authToken');
    }
  }
}


@NgModule({
  declarations: [
    MyApp,
    //Pages
    SplashPage,
    //Components
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    IonicImageViewerModule,
    DatePickerModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp),
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useFactory: jwtOptionsFactory
      },
      config: {
        whitelistedDomains: ['localhost:8080', 'sarraf.acculynksystems.com'],
        throwNoTokenError: false,
        skipWhenExpired: false
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    //Pages
    SplashPage,
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },

    //Native
    StatusBar,
    SplashScreen,
    File,
    Camera,
    FilePath,
    Base64,
    OneSignal,

    //Miscellaneous
    ValidatorProvider,
    AndroidPermissions,
    SmsServiceProvider,

    //Network
    UserNetServiceProvider,
    AccountsNetServiceProvider,
    BankNetServiceProvider,
    TelcoNetServiceProvider,
    TransactionsNetServiceProvider,
    //Database
    AccountsDbServiceProvider,
    BankDbServiceProvider,
    TelcoDbServiceProvider,
    PushNotificationProvider
  ],
  schemas: []
})
export class AppModule { }
