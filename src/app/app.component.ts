import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ModalController, MenuController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { JwtHelperService } from '@auth0/angular-jwt';
import { StatusBar } from '@ionic-native/status-bar';

import { SplashPage } from '../pages/splash/splash';
import { PushNotificationProvider } from '../providers/push-notification/push-notification';
import { AccountsDbServiceProvider } from '../providers/accounts-db-service/accounts-db-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any;
  activePage: any;
  isLoggedIn: boolean = false;
  introShown: boolean = false;
  userNames: string = null;
  userAvi: string = null;
  hasAvi: boolean = false;
  authToken: string = null;

  pages: Array<{ icon: string, title: string, component: any }>;

  constructor(public platform: Platform, public modalCtrl: ModalController, public statusBar: StatusBar, public menuCtrl: MenuController, private storage: Storage, private events: Events, private jwtHelper: JwtHelperService, private pushService: PushNotificationProvider, private accDBService: AccountsDbServiceProvider) {
    this.initNav();
    this.pushService.initPush();
    this.pages = [
      { icon: 'ios-home-outline', title: 'Home', component: 'HomePage' },
      { icon: 'ios-card-outline', title: 'Transactions', component: 'TransactionsPage' },
      { icon: 'ios-pricetag-outline', title: 'Pay Bills', component: 'UtilityPage' },
      { icon: 'ios-briefcase-outline', title: 'My Accounts', component: 'AccountsPage' },
      { icon: 'ios-person-outline', title: 'Profile', component: 'MySarrafPage' },
      //{ icon: 'ios-flask-outline', title: 'Test Page', component: 'SamplePage' },
      { icon: 'ios-log-out-outline', title: 'Logout', component: 'Logout' }
    ];
    this.activePage = this.pages[0];
  }

  initNav() {
    this.storage.get('introShown').then(res => {
      if (res === true) {
        this.introShown = true;
      } else {
        this.introShown = false;
      }
    });
    this.storage.get('isLoggedIn').then(res => {
      if (res === true) {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
    });
    this.storage.get('userNames').then(res => {
      this.userNames = res;
    });
    this.events.subscribe('username:changed', username => {
      if (username !== 'undefined' && username !== "") {
        this.userNames = username;
      }
    });
    this.storage.get('userAvi').then(res => {
      if (typeof res !== 'undefined' && res) {
        this.userAvi = res;
        this.hasAvi = true;
      }
    });
    this.events.subscribe('avi:changed', avi => {
      if (avi !== 'undefined' && avi !== "") {
        this.userAvi = avi;
        this.hasAvi = true;
      } else {
        this.hasAvi = false;
      }
    });
    this.storage.get('authToken').then(res => {
      if (typeof res !== 'undefined' && res) {
        this.authToken = res;
      }
      this.initializeApp();
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();
      this.statusBar.backgroundColorByHexString("#7300a5");
      this.statusBar.hide();

      //Init animated splash
      let splash = this.modalCtrl.create(SplashPage);
      splash.present().then(() => {
        this.checkAppStatus();
      });
    });
  }

  checkAppStatus() {
    this.menuCtrl.enable(false);
    if (this.introShown !== true) {
      this.rootPage = 'IntroPage';
    } else if (this.isLoggedIn === true && this.introShown === true) {
      this.checkTokenStatus();
    } else {
      this.rootPage = 'LoginPage';
    }
  }

  checkTokenStatus() {
    if ((this.authToken === null || typeof (this.authToken) === 'undefined' || this.authToken === '') || this.jwtHelper.isTokenExpired(this.authToken) === true) {
      this.logout();
    } else {
      setTimeout(() => {
        this.menuCtrl.enable(true);
        this.rootPage = 'HomePage';
      }, 3000);
    }
  }

  checkActive(page) {
    return page == this.activePage;
  }

  openPage(page) {
    if (page.component != 'Logout') {
      this.nav.setRoot(page.component);
      this.activePage = page;
    } else {
      this.logout();
    }
  }

  logout() {
    if (this.statusBar.isVisible) {
      this.statusBar.hide();
    }
    this.storage.set('isLoggedIn', false).then(() => {
      this.accDBService.deleteAll();
      this.storage.remove('userID');
      this.storage.remove('userNames');
      this.storage.remove('userAvi').then(() => {
        this.hasAvi = false;
      });
      this.storage.remove('authToken');
      this.activePage = this.pages[0];
      this.nav.setRoot('LoginPage');
    });
  }
}
