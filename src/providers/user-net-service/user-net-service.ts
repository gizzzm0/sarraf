import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class UserNetServiceProvider {
  //endpoint: string = 'http://localhost/acculynk/Sarraf/sarrafAPI/public/v1';
  endpoint: string = 'http://dev.surraf.com/api/public/v1';

  constructor(private http: HttpClient) {
  }

  //Signing in
  public signIn(creds) {
    let hdrs = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.post(this.endpoint + '/auth', creds, { headers: hdrs });
  }

  //Registration
  public signUp(details) {
    let hdrs = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.post(this.endpoint + '/register', details, { headers: hdrs });
  }

  //Registration Confirmation
  public confirm(passcode) {
    let hdrs = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.post(this.endpoint + '/confirm', passcode, { headers: hdrs });
  }

  //Request different OTP
  public reconfirm(pass) {
    //ToDo: Request for new OTP after 10 seconds countdown
  }

  //Password Reset
  public forgotPass(email) {
    let hdrs = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.post(this.endpoint + '/reset', email, { headers: hdrs });
  }

  //Get info
  public getUserData(authToken) {
    let hdrs = new HttpHeaders().set('Authorization', authToken);
    return this.http.get(this.endpoint + '/my_profile', { headers: hdrs });
  }

  //Edit info
  //Get OTP
  public getOTP(authToken, details) {
    let headersArr = {
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': authToken
    };
    let hdrs = new HttpHeaders(headersArr);
    return this.http.post(this.endpoint + '/edit_verification', details, { headers: hdrs });
  }
  //Save new profile
  public saveProfile(authToken, details) {
    let headersArr = {
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': authToken
    };
    let hdrs = new HttpHeaders(headersArr);
    return this.http.post(this.endpoint + '/edit_profile', details, { headers: hdrs });
  }
  //Upload avi
  public saveAVI(authToken, avi) {
    let headersArr = {
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': authToken
    };
    let hdrs = new HttpHeaders(headersArr);
    return this.http.post(this.endpoint + '/upload_avi', avi, { headers: hdrs });
  }
  //Save new password
  public savePasscode(authToken, pass) {
    let headersArr = {
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': authToken
    };
    let hdrs = new HttpHeaders(headersArr);
    return this.http.post(this.endpoint + '/change_pass', pass, { headers: hdrs });
  }
}
