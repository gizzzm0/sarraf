import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class TelcoNetServiceProvider {
  //endpoint: string = 'http://localhost/acculynk/Sarraf/sarrafAPI/public/v1';
  endpoint: string = 'http://dev.surraf.com/api/public/v1';

  constructor(public http: HttpClient) {
  }

  public getTelcos(authToken) {
    let hdrs = new HttpHeaders().set('Authorization', authToken);
    return this.http.get(this.endpoint + '/telcos', { headers: hdrs });
  }

}
