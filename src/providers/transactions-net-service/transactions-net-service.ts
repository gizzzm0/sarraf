import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class TransactionsNetServiceProvider {
  //endpoint: string = 'http://localhost/acculynk/Sarraf/sarrafAPI/public/v1';
  endpoint: string = 'http://dev.surraf.com/api/public/v1';

  constructor(public http: HttpClient) {
  }

  public getFX(authToken, sending, receiving) {
    let hdrs = new HttpHeaders().set('Authorization', authToken);
    let prms = new HttpParams().set('sendingID', sending).set('receivingID', receiving);
    return this.http.get(this.endpoint + '/get_fx_rate', { headers: hdrs, params: prms });
  }

  public getFXRate(authToken, sending, receiving) {
    let hdrs = new HttpHeaders().set('Authorization', authToken);
    let prms = new HttpParams().set('sendingCurr', sending).set('receivingCurr', receiving);
    return this.http.get(this.endpoint + '/get_fx_rate_main', { headers: hdrs, params: prms });
  }

  public sendMoney(authToken, txn) {
    let headersArr = {
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': authToken
    };
    let hdrs = new HttpHeaders(headersArr);
    return this.http.post(this.endpoint + '/send_money', txn, { headers: hdrs });
  }

  public completeTxn(authToken, creds) {
    let headersArr = {
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': authToken
    };
    let hdrs = new HttpHeaders(headersArr);
    return this.http.post(this.endpoint + '/complete_transfer', creds, { headers: hdrs });
  }

}
