import { Platform, Events } from 'ionic-angular';
import { Injectable, NgZone } from '@angular/core';
import { OneSignal } from '@ionic-native/onesignal';
import { Storage } from '@ionic/storage';

import { AccountsDbServiceProvider } from '../../providers/accounts-db-service/accounts-db-service';

@Injectable()
export class PushNotificationProvider {
  appId = '7d261983-8ece-45ca-9c2d-8b553459d5a7';
  googleProjectId = '698846862673';

  constructor(private platform: Platform, private notification: OneSignal, private storage: Storage, private accDBService: AccountsDbServiceProvider, private zone: NgZone, private events: Events) {
    this.initPush();
  }

  initPush() {
    this.platform.ready().then(() => {
      if (this.platform.is('cordova')) {

        this.notification.startInit(this.appId, this.googleProjectId);
        this.notification.inFocusDisplaying(this.notification.OSInFocusDisplayOption.Notification);
        this.storage.get('pushID').then(id => {
          if (typeof id === 'undefined' || id === '' || id === null) {
            this.notification.getIds().then((ids) => {
              this.storage.set('pushID', ids.userId);
            });
          }
        });
        this.notification.setSubscription(true);
        this.notification.handleNotificationReceived().subscribe((data) => {
          //alert('OneSignal Notification Received');
        });
        this.notification.handleNotificationOpened().subscribe((data) => {
          // Code after Notification opened
          let accountID = data['notification']['payload']['additionalData']['accID'];
          let accountAmt = data['notification']['payload']['additionalData']['accAmt'];
          if (typeof (data) !== 'undefined' && data) {
            this.accDBService.findAccount(accountID).then(acc => {
              let hasAccounts = Object.keys(acc).length;
              if (hasAccounts !== 0) {
                this.zone.run(() => {
                  let account = acc['docs'][0];
                  account.AmountAvailable = accountAmt;
                  this.accDBService.update(account);
                  this.accDBService.findDef().then(res => {
                    let acc = res['docs'][0];
                    this.events.publish('amtdefault:changed', this.formatCurrency(parseFloat(acc['AmountAvailable']).toFixed(2)));
                  });
                });
              }
            });
          }
        });
        this.notification.endInit();

      }
    });
  }

  formatCurrency(amt) {
    return amt.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
}
