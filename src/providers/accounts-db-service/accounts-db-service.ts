import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';
import PouchFind from 'pouchdb-find';
import cordovaSqlitePlugin from 'pouchdb-adapter-cordova-sqlite';
import 'rxjs/add/operator/map';
PouchDB.plugin(PouchFind);

@Injectable()
export class AccountsDbServiceProvider {
  private _db: any;
  //private _remote: any;
  //private apiKey: string = null;
  //private password: string = null;
  public _accounts;
  public _defaultAcc;

  constructor() {
    this.initDB();
  }

  initDB() {
    PouchDB.plugin(cordovaSqlitePlugin);
    this._db = new PouchDB('accounts.db', { adapter: 'cordova-sqlite' });
    //this._remote = 'http://localhost:5984/accounts';
    //this.apiKey = 'tlypsteneatemithendecion';
    //this.password = '308153889b4b901bedbfb19339300c61231df5f2';
    //this._remote = 'https://a5d8af6c-b86c-4346-95be-a5605ef2a42a-bluemix.cloudant.com/accounts';


    /* let options = {
      live: true,
      retry: true,
      continuous: true,
        auth: {
        username: this.apiKey,
        password: this.password
      }
    };

    this._db.sync(this._remote, options).on('change', (info) => {
      console.log('Handling syncing change');
    }).on('paused', (info) => {
      console.log('Handling syncing pause');
    }).on('active', (info) => {
      console.log('Handling syncing active');
    }).on('denied', (err) => {
      console.log('Handling syncing denied');
    }).on('complete', (info) => {
      console.log('Handling syncing complete');
    }).on('error', (err) => {
      console.log('Handling syncing error'); 
    });*/
  }

  add(account) {
    return this._db.post(account);
  }

  update(account) {
    return this._db.put(account);
  }

  delete(account) {
    return this._db.remove(account);
  }

  getAll() {
    if (!this._accounts) {
      return this._db.allDocs({
        include_docs: true
      }).then(docs => {
        this._accounts = docs.rows.map(row => {
          row.doc.AmountAvailable = this.formatCurrency(parseFloat(row.doc.AmountAvailable).toFixed(2));
          return row.doc;
        });

        this._db.changes({ live: true, since: 'now', include_docs: true })
          .on('change', this.onDatabaseChange);

        return this._accounts;
      });
    } else {
      return Promise.resolve(this._accounts);
    }
  }

  deleteAll() {
    //this._db.destroy();
    this._db.allDocs({ include_docs: true }).then(allDocs => {
      return allDocs.rows.map(row => {
        return { _id: row.id, _rev: row.doc._rev, _deleted: true };
      });
    }).then(deleteDocs => {
      return this._db.bulkDocs(deleteDocs);
    });
  }

  findDef() {
    return new Promise(resolve => {
      this._db.find({
        selector: { isDefault: 1 }
      }).then(res => {
        resolve(res);
      });
    })
  }

  findAccount(accID) {
    return new Promise(resolve => {
      this._db.find({
        selector: { accountID: accID }
      }).then(res => {
        resolve(res);
      });
    })
  }

  onDatabaseChange = (change) => {
    var idx = this.findIndex(this._accounts, change.id);
    var account = this._accounts[idx];

    if (change.deleted) {
      if (account) {
        this._accounts.splice(idx, 1);
      }
    } else {
      change.doc.Date = new Date(change.doc.Date);
      if (account && account._id === change.id) {
        this._accounts[idx] = change.doc;
      } else {
        this._accounts.splice(idx, 0, change.doc);
      }
    }
  }

  findIndex(array, id) {
    var low = 0, high = array.length, mid;
    while (low < high) {
      mid = (low + high) >>> 1;
      array[mid]._id < id ? low = mid + 1 : high = mid
    }
    return low;
  }

  formatCurrency(amt) {
    return amt.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
}
