import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable()
export class BankNetServiceProvider {
  //endpoint: string = 'http://localhost/acculynk/Sarraf/sarrafAPI/public/v1';
  endpoint: string = 'http://dev.surraf.com/api/public/v1';

  constructor(public http: HttpClient) {
  }

  public getBanks(authToken) {
    let hdrs = new HttpHeaders().set('Authorization', authToken);
    return this.http.get(this.endpoint + '/banks', { headers: hdrs });
  }

  public getBankBranches(authToken, bank) {
    let hdrs = new HttpHeaders().set('Authorization', authToken);
    let prms = new HttpParams().set('bankID', bank);
    return this.http.get(this.endpoint + '/bank_branches', { headers: hdrs, params: prms });
  }

}
