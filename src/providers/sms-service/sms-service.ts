import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { AndroidPermissions } from '@ionic-native/android-permissions';

declare var SMS: any;
@Injectable()
export class SmsServiceProvider {

  constructor(private androidPermissions: AndroidPermissions, private platform: Platform) {
    this.init();
  }

  init() {
    this.checkPermissions();
  }

  checkPermissions() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(success => {
      this.onSMSReceivedListener();
    }, err => {
      this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_SMS);
    });
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]);
  }

  onSMSReceivedListener() {
    return new Promise(resolve => {
      this.platform.ready().then((readySource) => {
        if (this.platform.is('cordova')) {
          if (this.platform.is('android')) {
            if (SMS) SMS.startWatch(() => {
              console.log('Listening...');
            }, Error => {
              console.log('Failed to start listener');
            });
            document.addEventListener('onSMSArrive', (e: any) => {
              var sms = e.data;
              resolve(sms);
            });
          }
        }
      });
    });
  }
}
