import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class AccountsNetServiceProvider {
  //endpoint: string = 'http://localhost/acculynk/Sarraf/sarrafAPI/public/v1';
  endpoint: string = 'http://dev.surraf.com/api/public/v1';

  constructor(public http: HttpClient) {
  }

  public addAccount(authToken, account) {
    let headersArr = {
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': authToken
    };
    let hdrs = new HttpHeaders(headersArr);
    return this.http.post(this.endpoint + '/new_account', account, { headers: hdrs });
  }

  public getAccount(authToken, accountid) {
    let hdrs = new HttpHeaders().set('Authorization', authToken);
    let prms = new HttpParams().set('accID', accountid);
    return this.http.get(this.endpoint + '/account_info', { headers: hdrs, params: prms });
  }

  public editAccount(authToken, account) {
    let headersArr = {
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': authToken
    };
    let hdrs = new HttpHeaders(headersArr);
    return this.http.post(this.endpoint + '/edit_account', account, { headers: hdrs });
  }

  public getUserAccounts(authToken, email) {
    let hdrs = new HttpHeaders().set('Authorization', authToken);
    let prms = new HttpParams().set('userEmail', email);
    return this.http.get(this.endpoint + '/user_txn_accounts', { headers: hdrs, params: prms });
  }

  public getAvailableBal(authToken, account) {
    let hdrs = new HttpHeaders().set('Authorization', authToken);
    let prms = new HttpParams().set('accID', account);
    return this.http.get(this.endpoint + '/account_balance', { headers: hdrs, params: prms });
  }
}
