import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccountsNewPage } from './accounts-new';

import { DatePickerModule } from 'ionic3-datepicker';

@NgModule({
    declarations: [
        AccountsNewPage,
    ],
    imports: [
        IonicPageModule.forChild(AccountsNewPage),
        DatePickerModule
    ]
})
export class AccountsNewPageModule { }
