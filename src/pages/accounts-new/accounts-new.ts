import { Component, ViewChild } from '@angular/core';
import { IonicPage, ViewController, ModalController, ToastController, LoadingController, Loading } from 'ionic-angular';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { DatePickerDirective } from 'ionic3-datepicker';

import { BankNetServiceProvider } from '../../providers/bank-net-service/bank-net-service';
import { TelcoNetServiceProvider } from '../../providers/telco-net-service/telco-net-service';
import { AccountsNetServiceProvider } from '../../providers/accounts-net-service/accounts-net-service';
import { AccountsDbServiceProvider } from '../../providers/accounts-db-service/accounts-db-service';

@IonicPage()
@Component({
  selector: 'page-accounts-new',
  templateUrl: 'accounts-new.html',
})
export class AccountsNewPage {
  @ViewChild(DatePickerDirective) private datepickerDirective: DatePickerDirective;
  accForm: FormGroup;
  loader: Loading;

  isBank: boolean = false;
  isBankSelected: boolean = false;
  isTelco: boolean = false;
  isPaypal: boolean = false;
  isCard: boolean = false;
  UID: number = null;
  authToken: string = null;

  banks = [];
  bankbranches = [];
  telcos = [];

  constructor(private viewCtrl: ViewController, private modalCtrl: ModalController, private toastCtrl: ToastController, private loadingCtrl: LoadingController, private dbService: AccountsDbServiceProvider, private banksNetService: BankNetServiceProvider, private telcoNetService: TelcoNetServiceProvider, private accountsNetService: AccountsNetServiceProvider, private formBuilder: FormBuilder, private storage: Storage) {
    this.accForm = this.formBuilder.group({
      type: new FormControl({ disabled: false, value: '' }, Validators.required),
      bankname: new FormControl({ disabled: true, value: '' }),
      bank: new FormControl({ disabled: false, value: 0 }, Validators.compose([Validators.pattern('[0-9]*'), Validators.required])),
      branchname: new FormControl({ disabled: true, value: '' }),
      branch: new FormControl({ disabled: false, value: 0 }, Validators.compose([Validators.pattern('[0-9]*'), Validators.required])),
      telconame: new FormControl({ disabled: true, value: '' }),
      telco: new FormControl({ disabled: false, value: 0 }, Validators.compose([Validators.pattern('[0-9]*'), Validators.required])),
      currency: new FormControl({ disabled: false, value: '' }, Validators.required),
      accnumber: new FormControl({ disabled: true, value: '' }, Validators.required),
      phonenumber: new FormControl({ disabled: true, value: '' }, Validators.required),
      ppemail: new FormControl({ disabled: false, value: '' }, Validators.compose([Validators.email, Validators.required])),
      cardnumber: new FormControl({ disabled: false, value: '' }, Validators.required),
      cvc: new FormControl({ disabled: false, value: '' }, Validators.required),
      cardexpiry: new FormControl({ disabled: false, value: '' }, Validators.required),
      name: new FormControl({ disabled: false, value: '' }, Validators.compose([Validators.maxLength(30), Validators.required])),
      pin: new FormControl({ disabled: false, value: '' }, Validators.compose([Validators.maxLength(120), Validators.pattern('[a-zA-Z0-9]*'), Validators.required])),
      cpin: new FormControl({ disabled: false, value: '' }, Validators.compose([Validators.maxLength(120), Validators.pattern('[a-zA-Z0-9]*'), Validators.required])),
      desc: new FormControl({ disabled: false, value: '' }, Validators.compose([Validators.maxLength(120)]))
    });
    this.storage.get('userID').then(res => {
      if (typeof res != 'undefined' && res) {
        this.UID = res;
      }
    });
    this.storage.get('authToken').then(res => {
      if (typeof res != 'undefined' && res) {
        this.authToken = res;
      }
    });
  }

  ionViewWillEnter() {
    let dateNow = new Date();
    this.datepickerDirective.value = dateNow;
    this.datepickerDirective.min = dateNow;
    this.datepickerDirective.locale = 'en-UK';
    this.datepickerDirective.markDates = [dateNow];
  }

  setDate(date: any) {
    let tzoffset = date.getTimezoneOffset() * 60000;
    let localISOTime = (new Date(date - tzoffset)).toISOString().slice(0, -1);
    this.accForm.get('cardexpiry').setValue(localISOTime.substring(0, 10));
  }

  saveAccount() {
    if (!this.accForm.valid) {
      this.createToast('Please fill in ALL fields and correct any highlighted ones.', 3000, 'bottom');
    } else {
      let pin = this.accForm.get('pin').value;
      let cpin = this.accForm.get('cpin').value;
      if (pin === cpin) {
        let formData = this.accForm.getRawValue();
        this.createLoading('Creating your account...');
        this.accountsNetService.addAccount(this.authToken, formData).subscribe(res => {
          if (res['success'] === true) {
            this.loader.dismiss().then(() => {
              this.createToast(res['msg'], 3000, 'bottom');
              this.saveAccountPouch(res['data']);
            });
          } else {
            this.loader.dismiss().then(() => {
              this.createToast(res['msg'], 3000, 'bottom');
            });
          }
        }, err => {
          this.loader.dismiss().then(() => {
            console.log(err);
            this.createToast('Oops!! Error creating your account. Please check your connectivity status and try again.', 4000, 'bottom');
          });
        });

      } else {
        this.createToast('Your PINs do not match!', 3000, 'bottom');
      }
    }
  }

  saveAccountPouch(acc) {
    this.dbService.add(acc);
    this.viewCtrl.dismiss('accSaved');
  }

  getBank() {
    let bankModal = this.modalCtrl.create('BankSearchPage', { bank_list: this.banks });
    bankModal.onDidDismiss(data => {
      if (typeof data !== 'undefined' && data && data.bID > 0) {
        //Load bank branches
        this.createLoading('Getting Bank Branches..');
        this.banksNetService.getBankBranches(this.authToken, data.bID).subscribe(res => {
          if (res['success'] === true) {
            this.loader.dismiss().then(() => {
              this.isBankSelected = true;
              this.bankbranches = res['data'];
            });
          } else {
            this.loader.dismiss().then(() => {
              this.createToast(res['msg'], 3000, 'bottom');
            });
          }
        }, err => {
          this.loader.dismiss().then(() => {
            this.createToast('Oops!! Error getting Bank Branches. Please check your connectivity status and try again.', 4000, 'bottom');
          });
        });
        this.accForm.get('bankname').reset({ disabled: true, value: data.bName });
        this.accForm.get('bank').reset({ disabled: false, value: data.bID });
      } else {
        this.accForm.get('bankname').reset({ disabled: true, value: '' });
        this.accForm.get('bank').reset({ disabled: false, value: 0 });
      }
    });
    bankModal.present();
  }

  getBranch() {
    let bankBranchModal = this.modalCtrl.create('BankBranchSearchPage', { bank_branches_list: this.bankbranches });
    bankBranchModal.onDidDismiss(data => {
      if (typeof data !== 'undefined' && data && data.brID > 0) {
        this.accForm.get('branchname').reset({ disabled: true, value: data.brName });
        this.accForm.get('branch').reset({ disabled: false, value: data.brID });
      } else {
        this.accForm.get('branchname').reset({ disabled: true, value: '' });
        this.accForm.get('branch').reset({ disabled: false, value: 0 });
      }
    });
    bankBranchModal.present();
  }

  getTelco() {
    let telcoModal = this.modalCtrl.create('TelcoSearchPage', { telco_list: this.telcos });
    telcoModal.onDidDismiss(data => {
      if (typeof data !== 'undefined' && data && data.tID > 0) {
        this.accForm.get('telconame').reset({ disabled: true, value: data.tName });
        this.accForm.get('telco').reset({ disabled: false, value: data.tID });
      } else {
        this.accForm.get('telconame').reset({ disabled: true, value: '' });
        this.accForm.get('telco').reset({ disabled: false, value: 0 });
      }
    });
    telcoModal.present();
  }

  setType(type) {
    this.accForm.get('accnumber').reset({ disabled: false, value: 0 });
    this.accForm.get('phonenumber').reset({ disabled: false, value: 0 });
    this.accForm.get('cardnumber').reset({ disabled: false, value: 0 });
    this.accForm.get('cvc').reset({ disabled: false, value: 0 });
    this.accForm.get('cardexpiry').reset({ disabled: false, value: 0 });
    this.accForm.get('ppemail').reset({ disabled: false, value: 'null@surraf.com' });
    this.accForm.get('desc').reset({ disabled: false, value: '' });
    if (type == 1) {
      this.isTelco = false;
      this.isCard = false;
      this.isPaypal = false;
      this.isBankSelected = false;
      this.isBank = true;

      this.accForm.get('telconame').reset({ disabled: true, value: '' });
      this.accForm.get('telco').reset({ disabled: false, value: 0 });
      this.accForm.get('accnumber').reset({ disabled: false, value: '' });
      //Get banks for country
      this.createLoading('Getting Banks..');
      this.banksNetService.getBanks(this.authToken).subscribe(res => {
        if (res['success'] === true) {
          this.loader.dismiss().then(() => {
            this.banks = res['data'];
          });
        } else {
          this.loader.dismiss().then(() => {
            this.createToast(res['msg'], 3000, 'bottom');
          });
        }
      }, err => {
        this.loader.dismiss().then(() => {
          this.createToast('Oops!! Error getting Banks. Please check your connectivity status and try again.', 4000, 'bottom');
        });
      });
    } else if (type == 2) {
      this.isBank = false;
      this.isBankSelected = false;
      this.isCard = false;
      this.isPaypal = false;
      this.isTelco = true;

      this.accForm.get('branchname').reset({ disabled: true, value: '' });
      this.accForm.get('branch').reset({ disabled: false, value: 0 });
      this.accForm.get('phonenumber').reset({ disabled: false, value: '' });
      //Get telcos for country
      this.createLoading('Getting Telcos..');
      this.telcoNetService.getTelcos(this.authToken).subscribe(res => {
        if (res['success'] === true) {
          this.loader.dismiss().then(() => {
            this.telcos = res['data'];
          });
        } else {
          this.loader.dismiss().then(() => {
            this.createToast(res['msg'], 3000, 'bottom');
          });
        }
      }, err => {
        this.loader.dismiss().then(() => {
          this.createToast('Oops!! Error getting Telcos. Please check your connectivity status and try again.', 4000, 'bottom');
        });
      });
    } else if (type == 3) {
      this.isBank = false;
      this.isBankSelected = false;
      this.isTelco = false;
      this.isCard = false;
      this.isPaypal = false;

      this.accForm.get('branchname').reset({ disabled: true, value: '' });
      this.accForm.get('branch').reset({ disabled: false, value: 0 });
      this.accForm.get('telconame').reset({ disabled: true, value: '' });
      this.accForm.get('telco').reset({ disabled: false, value: 0 });
      this.accForm.get('desc').reset({ disabled: true, value: 'Surraf Wallet Account' });
    } else if (type == 4) {
      this.isBank = false;
      this.isBankSelected = false;
      this.isTelco = false;
      this.isCard = true;
      this.isPaypal = false;

      this.accForm.get('branchname').reset({ disabled: true, value: '' });
      this.accForm.get('branch').reset({ disabled: false, value: 0 });
      this.accForm.get('telconame').reset({ disabled: true, value: '' });
      this.accForm.get('telco').reset({ disabled: false, value: 0 });
      this.accForm.get('cardnumber').reset({ disabled: false, value: '' });
      this.accForm.get('cvc').reset({ disabled: false, value: '' });
      this.accForm.get('cardexpiry').reset({ disabled: true, value: '' });
    } else if (type == 5) {
      this.isBank = false;
      this.isBankSelected = false;
      this.isTelco = false;
      this.isCard = false;
      this.isPaypal = true;

      this.accForm.get('ppemail').reset({ disabled: false, value: '' });
      this.accForm.get('branchname').reset({ disabled: true, value: '' });
      this.accForm.get('branch').reset({ disabled: false, value: 0 });
      this.accForm.get('telconame').reset({ disabled: true, value: '' });
      this.accForm.get('telco').reset({ disabled: false, value: 0 });
    }
  }

  createToast(msg: string, dur: number, pos: string, cls: boolean = false) {
    this.toastCtrl.create({
      message: msg,
      duration: dur,
      position: pos,
      showCloseButton: cls
    }).present();
  }

  createLoading(msg: string) {
    this.loader = this.loadingCtrl.create({
      spinner: 'dots',
      content: msg,
      dismissOnPageChange: true
    });
    this.loader.present();
  }

  doesExist(value) {
    if (value !== null && value !== '' && value !== ' ' && (typeof (value) !== 'undefined')) {
      return true;
    } else {
      return false;
    }
  }
}
