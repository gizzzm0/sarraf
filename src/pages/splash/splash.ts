import { Component } from '@angular/core';
import { Platform, ViewController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
  selector: 'page-splash',
  templateUrl: 'splash.html',
})
export class SplashPage {

  constructor(private platform: Platform, public viewCtrl: ViewController, private splashScreen: SplashScreen) {
  }

  ionViewDidEnter() {
    this.platform.ready().then(() => {
      this.splashScreen.hide();
      setTimeout(() => {
        this.viewCtrl.dismiss();
      }, 4000);
    });
  }

}
