import { Component } from '@angular/core';
import { IonicPage, ViewController, ToastController, AlertController, LoadingController, Events } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';

import { UserNetServiceProvider } from '../../providers/user-net-service/user-net-service';
import { AccountsDbServiceProvider } from '../../providers/accounts-db-service/accounts-db-service';
import { SmsServiceProvider } from '../../providers/sms-service/sms-service';

@IonicPage()
@Component({
  selector: 'page-password',
  templateUrl: 'password.html',
})
export class PasswordPage {
  pwForm: FormGroup;

  constructor(private formBuilder: FormBuilder, public viewCtrl: ViewController, private toastCtrl: ToastController, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private apiService: UserNetServiceProvider, private storage: Storage, private events: Events, private dbService: AccountsDbServiceProvider, private smsService: SmsServiceProvider) {
    this.pwForm = this.formBuilder.group({
      pin: ['', Validators.compose([Validators.pattern('[a-zA-Z0-9]*'), Validators.required])],
      password: ['', Validators.required],
      cpassword: ['', Validators.required]
    })
  }

  ionViewWillEnter() {
    this.listen();
  }

  listen() {
    this.smsService.onSMSReceivedListener().then(smsData => {
      if (smsData['address'] === 'AFRICASTKNG') {
        this.pwForm.get('pin').setValue(smsData['body'].substr(-8, 7));
      }
    });
  }

  verify() {
    let password: string = this.pwForm.get('password').value;
    let cpassword: string = this.pwForm.get('cpassword').value; {
      if (password !== cpassword) {
        this.createToast('Sorry, your passwords do not match!', 3000, 'bottom');
      } else {
        if (!this.pwForm.valid) {
          this.createToast('Please fill in ALL fields and correct any highlighted ones.', 3000, 'bottom');
        } else {
          let formData = this.pwForm.value;
          let loader = this.loadingCtrl.create({
            spinner: 'dots',
            content: 'Finishing up...',
            dismissOnPageChange: true
          });
          loader.present().then(() => {
            this.apiService.confirm(formData).subscribe(res => {
              let user = [];
              let accInfo = [];
              if (res['success'] === true) {
                //Set up user info
                user = res['data'];
                let username = user['lastName'] + ' ' + user['firstName'] + ' ' + user['middleName'];
                this.storage.set('isLoggedIn', true);
                this.storage.set('userID', user['id']);
                this.storage.set('userNames', username);
                this.events.publish('username:changed', username);
                this.storage.set('userAvi', user['userAvatar']);
                this.storage.set('authToken', user['token']);

                //Set up default user account
                accInfo = res['account'];
                this.dbService.add(accInfo).then(() => {
                  loader.dismiss().then(() => {
                    this.viewCtrl.dismiss('confSuccess');
                  });
                }).catch(console.error.bind(console));

              } else {
                loader.dismiss().then(() => {
                  this.alertCtrl.create({
                    message: res['msg'],
                    buttons: ['Dismiss']
                  }).present();
                })
              }
            }, err => {
              loader.dismiss().then(() => {
                this.createToast('Oops!! Sorry, error signing up. Please check your connectivity status and try again.', 4000, 'bottom');
              });
            });
          });
        }
      }
    }
  }

  createToast(msg: string, dur: number, pos: string) {
    this.toastCtrl.create({
      message: msg,
      duration: dur,
      position: pos
    }).present();
  }

}
