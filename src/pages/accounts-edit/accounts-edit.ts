import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController, ToastController, AlertController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';

import { Md5 } from 'ts-md5/dist/md5';

import { AccountsNetServiceProvider } from '../../providers/accounts-net-service/accounts-net-service';
import { AccountsDbServiceProvider } from '../../providers/accounts-db-service/accounts-db-service';

@IonicPage()
@Component({
  selector: 'page-accounts-edit',
  templateUrl: 'accounts-edit.html',
})
export class AccountsEditPage {
  accForm: FormGroup;
  authToken: string = null;
  pinHash: string = null;
  account: any = {};

  constructor(private navParams: NavParams, private viewCtrl: ViewController, private toastCtrl: ToastController, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private dbService: AccountsDbServiceProvider, private apiService: AccountsNetServiceProvider, private formBuilder: FormBuilder, private storage: Storage) {
    this.accForm = this.formBuilder.group({
      id: new FormControl({ disabled: true, value: '' }, Validators.required),
      type: new FormControl({ disabled: true, value: '' }, Validators.required),
      currency: new FormControl({ disabled: true, value: '' }, Validators.required),
      number: new FormControl({ disabled: true, value: '' }, Validators.required),
      name: new FormControl({ disabled: true, value: '' }, Validators.compose([Validators.maxLength(30), Validators.required])),
      pin: new FormControl({ disabled: false, value: '' }, Validators.compose([Validators.maxLength(120), Validators.pattern('[a-zA-Z0-9]*')])),
      cpin: new FormControl({ disabled: false, value: '' }, Validators.compose([Validators.maxLength(120), Validators.pattern('[a-zA-Z0-9]*')])),
      desc: new FormControl({ disabled: true, value: '' }, Validators.compose([Validators.maxLength(120)]))
    });
    this.storage.get('authToken').then(res => {
      if (typeof res != 'undefined' && res) {
        this.authToken = res;
      }
    });
  }

  ionViewDidLoad() {
    this.account = this.navParams.get('localAcc');
    let editAccount = this.navParams.get('acc');

    this.pinHash = editAccount.accountPIN;
    this.accForm.get('id').reset({ disabled: true, value: editAccount.accID });
    this.accForm.get('type').reset({ disabled: true, value: editAccount.accountTypeID });
    this.accForm.get('currency').reset({ disabled: true, value: editAccount.accountCurrencyID });

    if (editAccount.isDefaultAccount === 1) {
      this.accForm.get('number').reset({ disabled: true, value: editAccount.accountNumber });
      this.accForm.get('name').reset({ disabled: true, value: editAccount.accountName });
      this.accForm.get('desc').reset({ disabled: true, value: editAccount.accountDescription });
    } else {
      if (editAccount.accountTypeID === 3) {
        this.accForm.get('number').reset({ disabled: true, value: editAccount.accountNumber });
      } else {
        this.accForm.get('number').reset({ disabled: false, value: editAccount.accountNumber });
      }
      this.accForm.get('name').reset({ disabled: false, value: editAccount.accountName });
      this.accForm.get('desc').reset({ disabled: false, value: editAccount.accountDescription });
    }
  }

  saveAccount() {
    if (!this.accForm.valid) {
      this.createToast('Please fill in ALL fields and correct any highlighted ones.', 3000, 'bottom');
    } else {
      let formData = this.accForm.getRawValue();
      let pinAlert = this.alertCtrl.create({
        title: 'Enter Current Account PIN',
        inputs: [{
          name: 'currentpin',
          placeholder: 'Current PIN',
          type: 'password'
        }],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel'
          },
          {
            text: 'Confirm',
            handler: data => {
              if (Md5.hashStr(data.currentpin) === this.pinHash) {
                let pin = this.accForm.get('pin').value;
                let cpin = this.accForm.get('cpin').value;
                if (pin === cpin) {
                  let loader = this.loadingCtrl.create({
                    spinner: 'dots',
                    content: 'Saving account details...',
                    dismissOnPageChange: true
                  });
                  loader.present().then(() => {
                    this.apiService.editAccount(this.authToken, formData).subscribe(res => {
                      if (res['success'] === true) {
                        this.account.Currency = formData.currency;
                        this.account.Number = formData.number;
                        this.account.Name = formData.name;
                        this.account.accountTypeDesc = formData.desc;
                        this.dbService.update(this.account);
                        loader.dismiss().then(() => {
                          this.viewCtrl.dismiss({ success: true, info: res['msg'] });
                        });
                      } else {
                        loader.dismiss().then(() => {
                          this.createToast(res['msg'], 3000, 'bottom');
                        });
                      }
                    }, err => {
                      loader.dismiss().then(() => {
                        console.log(err);
                        this.createToast('Oops!! Error saving your account. Please check your connectivity status and try again.', 4000, 'bottom');
                      });
                    });

                  });
                } else {
                  this.createToast('Oops! Your new PINs DO NOT match. Please correct and resubmit.', 3000, 'bottom');
                }
              } else {
                this.createToast('Sorry! Wrong PIN.', 3000, 'bottom');
              }
            }
          }
        ]
      });
      pinAlert.present();
    }
  }

  createToast(msg: string, dur: number, pos: string) {
    this.toastCtrl.create({
      message: msg,
      duration: dur,
      position: pos
    }).present();
  }
}
