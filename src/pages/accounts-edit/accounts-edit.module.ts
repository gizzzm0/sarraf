import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccountsEditPage } from './accounts-edit';

@NgModule({
  declarations: [
    AccountsEditPage,
  ],
  imports: [
    IonicPageModule.forChild(AccountsEditPage),
  ],
})
export class AccountsEditPageModule {}
