import { Component, ViewChild } from '@angular/core';
import { IonicPage, Platform, NavController, AlertController, LoadingController, ModalController, ToastController, ActionSheetController, Events } from 'ionic-angular';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { DatePickerDirective } from 'ionic3-datepicker';
import { Md5 } from 'ts-md5/dist/md5';

import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { Base64 } from '@ionic-native/base64';

import { UserNetServiceProvider } from '../../providers/user-net-service/user-net-service';

declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-my-sarraf',
  templateUrl: 'my-sarraf.html',
})
export class MySarrafPage {
  @ViewChild(DatePickerDirective) private datepickerDirective: DatePickerDirective;
  userAvi: string = null;
  hasAvi: boolean = false;
  minZoom: number = 1;
  editForm: FormGroup;
  canEdit: boolean = false;
  canConfirm: boolean = false;
  authToken: string = null;
  passHash: string = null;

  constructor(private platform: Platform, public navCtrl: NavController, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private modalCtrl: ModalController, private toastCtrl: ToastController, public actionSheetCtrl: ActionSheetController, private camera: Camera, private file: File, private filePath: FilePath, private base64: Base64, private storage: Storage, private events: Events, private formBuilder: FormBuilder, private apiService: UserNetServiceProvider) {
    this.initHeader();
    this.editForm = this.formBuilder.group({
      firstname: new FormControl({ value: '', disabled: true }, Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])),
      middlename: new FormControl({ value: '', disabled: true }, Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])),
      surname: new FormControl({ value: '', disabled: true }, Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])),
      email: new FormControl({ value: '', disabled: true }, Validators.compose([Validators.email, Validators.required])),
      dob: new FormControl({ value: '', disabled: true }, Validators.required),
      countryname: new FormControl({ value: '', disabled: true }),
      countryid: new FormControl({ value: '', disabled: true }, Validators.compose([Validators.pattern('[0-9]*'), Validators.required, Validators.nullValidator])),
      countrycode: new FormControl({ value: '', disabled: true }, Validators.compose([Validators.required, Validators.nullValidator])),
      phone: new FormControl({ value: '', disabled: true }, Validators.compose([Validators.pattern('[0-9]*'), Validators.required])),
    });
    this.storage.get('authToken').then(res => {
      if (typeof res != 'undefined' && res) {
        this.authToken = res;
      }
    });
  }

  ionViewWillEnter() {
    let date18 = new Date();
    date18.setFullYear(date18.getFullYear() - 18);
    this.datepickerDirective.value = date18;
    this.datepickerDirective.max = date18;
    this.datepickerDirective.locale = 'en-UK';
    this.datepickerDirective.markDates = [date18];
  }

  ionViewDidEnter() {
    this.storage.get('uInfo').then(userInfo => {
      if (typeof (userInfo) !== 'undefined' && userInfo) {
        this.populateForm(JSON.parse(userInfo), true);
      }
    });
  }

  modPasscode() {
    let passcodeAlert = this.alertCtrl.create({
      title: 'Change Your Passcode',
      inputs: [
        {
          name: 'currentpin',
          placeholder: 'Current Passcode',
          type: 'password'
        },
        {
          name: 'newpin',
          placeholder: 'New Passcode',
          type: 'password'
        },
        {
          name: 'confirmpin',
          placeholder: 'Confirm Passcode',
          type: 'password'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Confirm',
          handler: data => {
            if (data.newpin === data.confirmpin) {
              let loader = this.loadingCtrl.create({
                spinner: 'dots',
                content: 'Changing PassCode...',
                dismissOnPageChange: true
              });
              loader.present().then(() => {
                this.apiService.savePasscode(this.authToken, { currentpin: data.currentpin, newpin: data.newpin }).subscribe(res => {
                  if (res['success'] === true) {
                    loader.dismiss().then(() => {
                      this.passHash = Md5.hashStr(data.newpin).toString();
                      this.createToast(res['msg'], 3000, 'bottom', true);
                    });
                  } else {
                    loader.dismiss().then(() => {
                      this.createToast(res['msg'], 3000, 'bottom', true);
                    });
                  }
                }, err => {
                  loader.dismiss().then(() => {
                    console.log(err);
                    this.createToast('Oops!! Error changing your Passcode. Please check your connectivity status and try again.', 4000, 'bottom');
                  });
                });
              });
            } else {
              this.createToast('Sorry! Wrong your passcodes DO NOT match.', 3000, 'bottom');
            }
          }
        }
      ]
    });
    passcodeAlert.present();
  }

  modAvi() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use device Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  takePicture(sourceType) {
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    this.camera.getPicture(options).then(imagePath => {
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath).then(filePath => {
          let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
          let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
          this.copyFileToLocalDir(correctPath, currentName, this.newImageName());
        });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.newImageName());
      }
    }, err => {
      this.createToast('Error while getting image.', 3000, 'bottom');
    });
  }

  copyFileToLocalDir(namePath, currentName, newName, newDir: any = cordova.file.dataDirectory) {
    this.file.copyFile(namePath, currentName, newDir, newName).then(success => {
      this.uploadAVI(newName);
    }, error => {
      this.createToast('Error while storing file.', 3000, 'bottom');
    });
  }

  uploadAVI(avatar: string) {
    let imagePath = this.imagePath(avatar);
    this.base64.encodeFile(imagePath).then(encAvi => {
      let loader = this.loadingCtrl.create({
        spinner: 'dots',
        content: 'Uploading new avatar...',
        dismissOnPageChange: false
      });
      loader.present().then(() => {
        this.apiService.saveAVI(this.authToken, { 'aviEncode': encAvi }).subscribe(res => {
          if (res['success'] === true) {
            loader.dismiss().then(() => {
              this.createToast(res['msg'], 3000, 'bottom', true);
              this.triggerEdit(false);
              this.initHeader();
            })
          } else {
            loader.dismiss().then(() => {
              this.createToast('Sorry! Couldn\'t change your avatar! Please try again', 3000, 'bottom');
            });
          }
        }, err => {
          loader.dismiss().then(() => {
            this.createToast('Oops!! Error changing your avatar. Please check your connectivity status and try again.', 4000, 'bottom');
          });
        });
      });
    })
  }

  newImageName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  imagePath(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  setDate(date: any) {
    if (this.canEdit === true) {
      let tzoffset = date.getTimezoneOffset() * 60000; //offset in milliseconds
      let localISOTime = (new Date(date - tzoffset)).toISOString().slice(0, -1);
      this.editForm.get('dob').setValue(localISOTime.substring(0, 10));
    }
  }

  triggerEdit(edit: boolean = true) {
    let loader = this.loadingCtrl.create({
      spinner: 'dots',
      content: 'Fetching updated info...',
      dismissOnPageChange: true
    });
    loader.present().then(() => {
      this.apiService.getUserData(this.authToken).subscribe(userInfo => {
        let user = [];
        if (userInfo['success'] === true) {
          user = userInfo['udata'];
          this.canEdit = edit;
          //Set local storage, scope vars and form inputs for new user info 
          this.storage.set('uInfo', JSON.stringify(user));
          let username = user['lastName'] + ' ' + user['firstName'] + ' ' + user['middleName'];
          this.storage.set('userNames', username).then(() => {
            this.events.publish('username:changed', username);
          });
          this.passHash = user['passHash'];
          this.populateForm(user, false);
          loader.dismiss();
        } else {
          loader.dismiss().then(() => {
            this.createToast(userInfo['msg'], 3000, 'bottom', true);
          });
        }
      }, err => {
        loader.dismiss().then(() => {
          this.createToast('Oops!! Sorry, error fetching data. Please check your connectivity status and try again.', 4000, 'bottom');
        });
      });
    });
  }

  populateForm(user: any[], status: boolean) {
    this.editForm.get('firstname').reset({ disabled: status, value: user['firstName'] });
    this.editForm.get('middlename').reset({ disabled: status, value: user['middleName'] });
    this.editForm.get('surname').reset({ disabled: status, value: user['lastName'] });
    this.editForm.get('email').reset({ disabled: status, value: user['userEmail'] });
    this.editForm.get('dob').reset({ disabled: status, value: user['dateOfBirth'] });
    this.editForm.get('countryid').reset({ disabled: status, value: user['countryID'] });
    this.editForm.get('countrycode').reset({ disabled: status, value: user['countryCode'] });
    this.editForm.get('countryname').reset({ disabled: status, value: user['countryName'] });
    this.editForm.get('phone').reset({ disabled: status, value: user['phoneNumber'] });
    let fullname = user['lastName'] + ' ' + user['firstName'] + ' ' + user['middleName'];
    this.storage.set('userNames', fullname).then(() => {
      this.events.publish('username:changed', fullname);
    });
    this.storage.set('userAvi', user['userAvatar']).then(() => {
      this.userAvi = user['userAvatar'];
      this.events.publish('avi:changed', user['userAvatar']);
    });
  }

  save() {
    if (!this.editForm.valid) {
      this.createToast('Please fill in ALL fields and correct any highlighted ones.', 3000, 'bottom');
    } else {
      //Enter current password first
      this.verifyUserPassword();
    }
  }

  verifyUserPassword() {
    let pinAlert = this.alertCtrl.create({
      title: 'Enter Current Password',
      inputs: [{
        name: 'currentpin',
        placeholder: 'Current Password',
        type: 'password'
      }],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Confirm',
          handler: data => {
            if (Md5.hashStr(data.currentpin) === this.passHash) {
              //Send one time PIN
              this.sendOTP();
            } else {
              this.createToast('Sorry! Wrong Password.', 3000, 'bottom');
            }
          }
        }
      ]
    });
    pinAlert.present();
  }

  sendOTP() {
    //Get OTP to prove New Phone Number is client's.
    let userData = [];
    userData.push({
      'newFirstName': this.editForm.get('firstname').value,
      'newPhoneNo': this.editForm.get('countrycode').value + '' + this.editForm.get('phone').value,
      'newEmail': this.editForm.get('email').value
    });
    let loader = this.loadingCtrl.create({
      spinner: 'dots',
      content: 'Fetching Activation Code...',
      dismissOnPageChange: true
    });
    loader.present().then(() => {
      this.apiService.getOTP(this.authToken, userData).subscribe(data => {
        if (data['success'] === true) {
          this.storage.set('otpHash', data['otpHash']).then(() => {
            this.editForm.disable();
            this.canEdit = false;
            this.canConfirm = true;
            loader.dismiss().then(() => {
              this.confirmOTP();
            });
          });
        } else {
          loader.dismiss().then(() => {
            this.createToast(data['msg'], 3000, 'bottom');
          });
        }
      }, err => {
        loader.dismiss().then(() => {
          this.createToast('Oops!! Error getting your Surraf Activation Code. Please check your connectivity status and try again.', 4000, 'bottom');
        });
      });
    });
  }

  confirmOTP() {
    this.alertCtrl.create({
      title: 'Activation',
      message: 'Enter the Activation PIN sent to you via SMS',
      inputs: [{
        name: 'otpin',
        placeholder: 'One Time Pin',
        type: 'text'
      }],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Confirm',
          handler: data => {
            this.storage.get('otpHash').then(otp => {
              if (Md5.hashStr(data.otpin) === otp) {
                //Call update API
                this.updateProfile();
              } else {
                this.createToast('Sorry! Wrong Activation PIN. Please try again.', 3000, 'bottom');
              }
            });
          }
        }
      ]
    }).present();
  }

  updateProfile() {
    let formData = this.editForm.value;
    let loader = this.loadingCtrl.create({
      spinner: 'dots',
      content: 'Updating Profile...',
      dismissOnPageChange: true
    });
    loader.present().then(() => {
      this.apiService.saveProfile(this.authToken, formData).subscribe(res => {
        if (res['success'] === true) {
          this.storage.remove('otpHash').then(() => {
            this.createToast(res['msg'], 3000, 'bottom', true);
            this.canConfirm = false;
            loader.dismiss().then(() => {
              this.triggerEdit(false);
            });
          });
        } else {
          loader.dismiss().then(() => {
            this.createToast(res['msg'], 3000, 'bottom', true);
          });
        }
      }, err => {
        loader.dismiss().then(() => {
          this.createToast('Oops!! Error saving your info. Please check your connectivity status and try again.', 4000, 'bottom');
        });
      });
    });
  }

  initHeader() {
    this.storage.get('userAvi').then(res => {
      if (typeof res !== 'undefined' && res) {
        this.userAvi = res;
        this.hasAvi = true;
        this.events.publish('avi:changed', res);
      }
    });
  }

  getCountry() {
    if (this.canEdit === true) {
      let countryModal = this.modalCtrl.create('CountrySearchPage');
      countryModal.onDidDismiss(data => {
        if (typeof data !== 'undefined' && data) {
          this.editForm.get('countryname').setValue(data.cName);
          this.editForm.get('countryid').setValue(data.cID);
          if ((typeof data.cCode !== 'undefined') && data.cCode) {
            this.editForm.get('countrycode').setValue(data.cCode);
          } else {
            this.createToast('Sorry! Your country calling code is missing. It\'s required for mobile confirmation. Please contact Sarraf admin.', 3000, 'top');
          }
        } else {
          this.editForm.get('countryname').setValue('');
          this.editForm.get('countryid').setValue('');
          this.editForm.get('countrycode').setValue('');
        }
      });
      countryModal.present();
    }
  }

  createToast(msg: string, dur: number, pos: string, cls: boolean = false) {
    this.toastCtrl.create({
      message: msg,
      duration: dur,
      position: pos,
      showCloseButton: cls
    }).present();
  }
}
