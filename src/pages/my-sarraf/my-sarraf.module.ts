import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MySarrafPage } from './my-sarraf';

import { IonicImageViewerModule } from 'ionic-img-viewer';
import { DatePickerModule } from 'ionic3-datepicker';
import { ExpandableHeaderComponent } from '../../components/expandable-header/expandable-header';

@NgModule({
    declarations: [
        MySarrafPage,
        ExpandableHeaderComponent
    ],
    imports: [
        IonicPageModule.forChild(MySarrafPage),
        IonicImageViewerModule,
        DatePickerModule
    ],
    schemas: [
    ]
})
export class MySarrafPageModule { }
