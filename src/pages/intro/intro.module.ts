import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IntroPage } from './intro';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
    declarations: [
        IntroPage,
    ],
    imports: [
        IonicPageModule.forChild(IntroPage),
        LottieAnimationViewModule.forRoot()
    ],
})
export class IntroPageModule { }
