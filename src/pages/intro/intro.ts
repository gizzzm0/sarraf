import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html'
})
export class IntroPage {
  @ViewChild(Slides) slides: Slides;
  skipMsg: string = "Skip";
  anim: any;

  transfer = { path: 'assets/animations/lottie/antenna.json' };
  transfer2 = { path: 'assets/animations/lottie/antenna.json' };
  pay = { path: 'assets/animations/lottie/walletcoin.json' };
  withdraw = { path: 'assets/animations/lottie/atm_link.json' };
  history = { path: 'assets/animations/lottie/tibetan-monk.json' };

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {
  }

  skip() {
    this.storage.set('introShown', true).then(() => {
      this.navCtrl.setRoot('LoginPage');
    })
  }

  slideChanged() {
    if (this.slides.getActiveIndex() >= this.slides.getPreviousIndex() && this.slides.isEnd()) {
      this.skipMsg = "Got it, thanks!!";
    } else {
      this.skipMsg = "Skip";
    }
  }

}
