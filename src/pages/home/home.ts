import { Component } from '@angular/core';
import { IonicPage, Events, NavController, ToastController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { StatusBar } from '@ionic-native/status-bar';
import { Storage } from '@ionic/storage';

import { AccountsDbServiceProvider } from '../../providers/accounts-db-service/accounts-db-service';
import { TransactionsNetServiceProvider } from '../../providers/transactions-net-service/transactions-net-service';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  fxForm: FormGroup;
  authToken: string = null;
  currDefault: string = null;
  amtDefault: number = null;

  constructor(public statusBar: StatusBar, public navCtrl: NavController, private toastCtrl: ToastController, private loadingCtrl: LoadingController, private storage: Storage, private formBuilder: FormBuilder, private accDBService: AccountsDbServiceProvider, private txnNetService: TransactionsNetServiceProvider, private events: Events) {
    this.init();
    this.fxForm = this.formBuilder.group({
      sendCurr: new FormControl({ disabled: false, value: '' }, Validators.compose([Validators.pattern('[A-Z]*'), Validators.required])),
      sendAmt: new FormControl({ disabled: false, value: 0 }, Validators.compose([Validators.pattern('[0-9.]*'), Validators.required])),
      recCurr: new FormControl({ disabled: false, value: '' }, Validators.compose([Validators.pattern('[A-Z]*'), Validators.required])),
      recAmt: new FormControl({ disabled: false, value: 0 }, Validators.compose([Validators.pattern('[0-9.]*'), Validators.required]))
    });
    this.storage.get('authToken').then(res => {
      if (typeof res != 'undefined' && res) {
        this.authToken = res;
      }
    });
  }

  init() {
    this.accDBService.findDef().then(res => {
      let acc = res['docs'][0];
      this.currDefault = acc['Currency'];
      this.amtDefault = this.formatCurrency(parseFloat(acc['AmountAvailable']).toFixed(2));
      this.events.subscribe('amtdefault:changed', def => {
        if (def !== 'undefined' && def !== "") {
          this.amtDefault = def;
        }
      });
    }).catch(err => {
      console.log(err);
    });
  }

  calcFx() {
    if (!this.fxForm.valid) {
      this.createToast('Please fill in ALL fields correctly.', 3000, 'bottom');
    } else {
      let toSend = this.fxForm.get('sendCurr').value;
      let toRec = this.fxForm.get('recCurr').value;
      let sendAmt = this.fxForm.get('sendAmt').value;
      if (toSend === toRec) {
        this.fxForm.get('recAmt').setValue(sendAmt);
      } else {
        let loader = this.loadingCtrl.create({
          spinner: 'dots',
          content: 'Getting forex exchange rate...',
          dismissOnPageChange: true
        });
        loader.present().then(() => {
          this.txnNetService.getFXRate(this.authToken, toSend, toRec).subscribe(res => {
            if (res['success'] === true) {
              loader.dismiss().then(() => {
                let fxRate = parseFloat(res['data'].rate);
                let calculated = sendAmt / fxRate;
                this.fxForm.get('recAmt').setValue(calculated.toFixed(2));
              });
            } else {
              loader.dismiss().then(() => {
                this.createToast(res['msg'], 3000, 'bottom');
              });
            }
          }, err => {
            loader.dismiss().then(() => {
              this.createToast('Oops!! Error getting rate. Please check your connectivity status and try again.', 4000, 'bottom');
            });
          });
        });
      }
    }
  }

  ionViewDidEnter() {
    if (!this.statusBar.isVisible) {
      this.statusBar.show();
    }
  }



  quickLink(pageName: string) {
    if (pageName === 'send') {
      this.navCtrl.push('TransfersPage');
    } else if (pageName === 'withdraw') {

    } else if (pageName === 'topup') {

    } else if (pageName === 'paybill') {
      this.navCtrl.push('UtilityPage');
    } else if (pageName === 'history') {

    } else if (pageName === 'accounts') {
      this.navCtrl.push('AccountsPage');
    }
  }

  createToast(msg: string, dur: number, pos: string) {
    this.toastCtrl.create({
      message: msg,
      duration: dur,
      position: pos
    }).present();
  }

  formatCurrency(amt) {
    return amt.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
}
