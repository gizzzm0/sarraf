import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignUpPage } from './sign-up';

import { DatePickerModule } from 'ionic3-datepicker';

@NgModule({
    declarations: [
        SignUpPage,
    ],
    imports: [
        IonicPageModule.forChild(SignUpPage),
        DatePickerModule
    ]
})
export class SignUpPageModule { }
