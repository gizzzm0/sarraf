import { Component, ViewChild } from '@angular/core';
import { IonicPage, ViewController, ModalController, ToastController, AlertController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { DatePickerDirective } from 'ionic3-datepicker';

import { UserNetServiceProvider } from '../../providers/user-net-service/user-net-service';

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {
  @ViewChild(DatePickerDirective) private datepickerDirective: DatePickerDirective;
  regForm: FormGroup;

  constructor(public viewCtrl: ViewController, private modalCtrl: ModalController, private formBuilder: FormBuilder, private toastCtrl: ToastController, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private apiService: UserNetServiceProvider, private storage: Storage) {
    this.regForm = this.formBuilder.group({
      firstname: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      middlename: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      surname: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      email: ['', Validators.compose([Validators.email, Validators.required])],
      dob: ['', Validators.required],
      countryname: [''],
      countryid: ['', Validators.compose([Validators.pattern('[0-9]*'), Validators.required, Validators.nullValidator])],
      idpp: ['', Validators.compose([Validators.pattern('[a-zA-Z0-9]*'), Validators.required])],
      countrycode: ['', Validators.compose([Validators.required, Validators.nullValidator])],
      phone: ['', Validators.compose([Validators.pattern('[0-9]*'), Validators.required])],
      currency: ['', Validators.compose([Validators.pattern('[a-zA-Z0-9]*'), Validators.required])],
      terms: [false],
      pushID: ['', Validators.required]
    });
  }

  ionViewWillEnter() {
    let date18 = new Date();
    date18.setFullYear(date18.getFullYear() - 18);
    this.datepickerDirective.value = date18;
    this.datepickerDirective.max = date18;
    this.datepickerDirective.locale = 'en-UK';
    this.datepickerDirective.markDates = [date18];
  }

  setDate(date: any) {
    let tzoffset = date.getTimezoneOffset() * 60000; //offset in milliseconds
    let localISOTime = (new Date(date - tzoffset)).toISOString().slice(0, -1);
    this.regForm.get('dob').setValue(localISOTime.substring(0, 10));
  }

  getCountry() {
    let countryModal = this.modalCtrl.create('CountrySearchPage');
    countryModal.onDidDismiss(data => {
      if (typeof data !== 'undefined' && data) {
        this.regForm.get('countryname').setValue(data.cName);
        this.regForm.get('countryid').setValue(data.cID);
        if ((typeof data.cCode !== 'undefined') && data.cCode) {
          this.regForm.get('countrycode').setValue(data.cCode);
        } else {
          this.createToast('Sorry! Your country calling code is missing. It\'s required for mobile confirmation. Please contact Sarraf admin.', 3000, 'top');
        }
      } else {
        this.regForm.get('countryname').setValue('');
        this.regForm.get('countryid').setValue('');
        this.regForm.get('countrycode').setValue('');
      }
    });
    countryModal.present();
  }

  openTnC() {
    let tncAlert = this.alertCtrl.create({
      title: 'Terms and Conditions',
      message: '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p> <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
      buttons: ['Dismiss']
    });
    tncAlert.present();
  }

  signUp() {
    this.storage.get('pushID').then(id => {
      if (typeof id !== 'undefined' && id !== null && id !== '') {
        this.regForm.get('pushID').setValue(id);
        let terms: boolean = this.regForm.get('terms').value;
        if (terms) {
          if (!this.regForm.valid) {
            this.createToast('Please fill in ALL fields and correct any highlighted ones.', 3000, 'bottom');
          } else {
            let formData = this.regForm.value;
            let loader = this.loadingCtrl.create({
              spinner: 'dots',
              content: 'Signing in...',
              dismissOnPageChange: true
            });
            loader.present().then(() => {
              this.apiService.signUp(formData).subscribe(res => {
                if (res['success'] === true) {
                  loader.dismiss().then(() => {
                    this.viewCtrl.dismiss('regSuccess');
                  });
                } else {
                  loader.dismiss().then(() => {
                    this.createToast(res['msg'], 3000, 'bottom', true);
                  })
                }
              }, err => {
                loader.dismiss().then(() => {
                  this.createToast('Oops!! Error signing up. Please check your connectivity status and try again.', 4000, 'bottom');
                });
              });
            });
          }
        } else {
          this.createToast('You MUST read and accept our Terms and Conditions to proceed.', 3000, 'bottom');
        }
      } else {
        this.createToast('Error setting PushID. Please check your connection and try again.', 3000, 'bottom');
      }
    });
  }

  createToast(msg: string, dur: number, pos: string, btn: boolean = false) {
    this.toastCtrl.create({
      message: msg,
      duration: dur,
      position: pos,
      showCloseButton: btn
    }).present();
  }
}
