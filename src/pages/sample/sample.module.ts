import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SamplePage } from './sample';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    SamplePage,
  ],
  imports: [
    IonicPageModule.forChild(SamplePage),
    LottieAnimationViewModule.forRoot()
  ],
})
export class SamplePageModule { }
