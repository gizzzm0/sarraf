import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavParams, ViewController, Searchbar } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-telco-search',
  templateUrl: 'telco-search.html',
})
export class TelcoSearchPage {
  @ViewChild('mainSearchbar') searchBar: Searchbar;
  telcosRaw = [];
  telcosFiltered = [];

  constructor(private viewCtrl: ViewController, private params: NavParams) {
    this.telcosRaw = this.params.get('telco_list');
  }

  ionViewDidEnter() {
    setTimeout(() => {
      this.searchBar.setFocus();
      this.searchBar.placeholder = 'Search telco name...';
      this.searchBar.autocorrect = 'on';
    }, 100);
  }
  selectTelco(telcoID, telcoName) {
    let res = { 'tID': telcoID, 'tName': telcoName };
    this.viewCtrl.dismiss(res);
  }


  filterTelcos(ev: any) {
    this.telcosFiltered = [];
    let val = ev.target.value;

    if (val && val.trim() != '') {
      this.telcosRaw.filter(telco => {
        if (telco["telcoName"].match(new RegExp(val, "i"))) {
          this.telcosFiltered.push({
            telcoid: telco["telcoID"],
            telconame: telco["telcoName"]
          });
        }
      });
    } else {
      this.telcosFiltered = [];
    }
  }
}
