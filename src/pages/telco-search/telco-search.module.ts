import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TelcoSearchPage } from './telco-search';

@NgModule({
  declarations: [
    TelcoSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(TelcoSearchPage),
  ]
})
export class TelcoSearchPageModule { }
