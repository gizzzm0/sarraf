import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavParams, ViewController, Searchbar } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-bank-search',
  templateUrl: 'bank-search.html',
})
export class BankSearchPage {
  @ViewChild('mainSearchbar') searchBar: Searchbar;
  banksRaw = [];
  banksFiltered = [];

  constructor(private viewCtrl: ViewController, private params: NavParams) {
    this.banksRaw = this.params.get('bank_list');
  }

  ionViewDidEnter() {
    setTimeout(() => {
      this.searchBar.setFocus();
      this.searchBar.placeholder = 'Search bank name...';
      this.searchBar.autocorrect = 'on';
    }, 100);
  }

  selectBank(bankID, bankName) {
    let res = { 'bID': bankID, 'bName': bankName };
    this.viewCtrl.dismiss(res);
  }

  filterBanks(ev: any) {
    this.banksFiltered = [];
    let val = ev.target.value;

    if (val && val.trim() != '') {
      this.banksRaw.filter(bank => {
        if (bank["bankName"].match(new RegExp(val, "i"))) {
          this.banksFiltered.push({
            bankid: bank["bankID"],
            bankname: bank["bankName"]
          });
        }
      });
    } else {
      this.banksFiltered = [];
    }
  }
}
