import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BankSearchPage } from './bank-search';

@NgModule({
  declarations: [
    BankSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(BankSearchPage),
  ]
})
export class BankSearchPageModule { }
