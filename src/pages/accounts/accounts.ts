import { Component, NgZone } from '@angular/core';
import { IonicPage, Platform, ModalController, ToastController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { AccountsDbServiceProvider } from '../../providers/accounts-db-service/accounts-db-service';
import { AccountsNetServiceProvider } from '../../providers/accounts-net-service/accounts-net-service';

@IonicPage()
@Component({
  selector: 'page-accounts',
  templateUrl: 'accounts.html',
})
export class AccountsPage {
  accounts = [];
  authToken: string = null;

  constructor(private platform: Platform, private modalCtrl: ModalController, private toastCtrl: ToastController, private loadingCtrl: LoadingController, private dbService: AccountsDbServiceProvider, private apiService: AccountsNetServiceProvider, private zone: NgZone, private storage: Storage) {
    this.storage.get('authToken').then(res => {
      if (typeof res != 'undefined' && res) {
        this.authToken = res;
      }
    });
  }

  ionViewDidLoad() {
    this.platform.ready().then(() => {
      this.dbService.getAll().then(data => {
        let hasAccounts = Object.keys(data).length;
        if (hasAccounts !== 0) {
          this.zone.run(() => {
            this.accounts = data;
            //alert(JSON.stringify(data));
          });
        } else {
          //ToDo - Show no accounts
        }
      }).catch(console.error.bind(console));
    });
  }

  addAccount() {
    let addModal = this.modalCtrl.create('AccountsNewPage');
    addModal.present();
  }

  editAccount(account) {
    //Get account details
    let loader = this.loadingCtrl.create({
      spinner: 'dots',
      content: 'Fetching account details...',
      dismissOnPageChange: true
    });
    loader.present().then(() => {
      this.apiService.getAccount(this.authToken, account.accountID).subscribe(res => {
        if (res['success'] === true) {
          loader.dismiss().then(() => {
            let editModal = this.modalCtrl.create('AccountsEditPage', { acc: res['data'], localAcc: account });
            editModal.onDidDismiss((data: string) => {
              if (typeof (data) !== 'undefined') {
                if (data['success'] === true) {
                  this.createToast(data['info'], 3000, 'bottom');
                } else {
                  this.createToast('The account\'s details were not modified.', 3000, 'bottom');
                }
              } else {
                this.createToast('The account\'s details were not modified.', 3000, 'bottom');
              }
            });
            editModal.present();
          });
        } else {
          loader.dismiss().then(() => {
            this.createToast(res['msg'], 3000, 'bottom');
          });
        }
      }, err => {
        loader.dismiss().then(() => {
          console.log(err);
          this.createToast('Oops!! Error getting account information. Please check your connectivity status and try again.', 4000, 'bottom');
        });
      });
    });
  }

  createToast(msg: string, dur: number, pos: string) {
    this.toastCtrl.create({
      message: msg,
      duration: dur,
      position: pos
    }).present();
  }
}
