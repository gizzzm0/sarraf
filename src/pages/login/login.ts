import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, ModalController, ToastController, AlertController, LoadingController, Events } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';

import { StatusBar } from '@ionic-native/status-bar';

import { UserNetServiceProvider } from '../../providers/user-net-service/user-net-service';
import { AccountsDbServiceProvider } from '../../providers/accounts-db-service/accounts-db-service';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loginForm: FormGroup;

  constructor(public navCtrl: NavController, public menuCtrl: MenuController, public modalCtrl: ModalController, public statusBar: StatusBar, private formBuilder: FormBuilder, private toastCtrl: ToastController, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private apiService: UserNetServiceProvider, private storage: Storage, private events: Events, private accDBService: AccountsDbServiceProvider) {
    this.loginForm = this.formBuilder.group({
      phone: ['', Validators.compose([Validators.pattern('[0-9]*'), Validators.required])],
      passcode: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    this.storage.get('lastLogin').then(number => {
      if (typeof (number) !== 'undefined' && number) {
        this.loginForm.get('phone').setValue(number);
      }
    });
  }

  signUp() {
    let signUpModal = this.modalCtrl.create('SignUpPage');
    signUpModal.onWillDismiss((data: string) => {
      if (data == 'regSuccess') {
        this.setPassword();
      }
    });
    signUpModal.present();
  }

  setPassword() {
    let passwordModal = this.modalCtrl.create('PasswordPage');
    passwordModal.onDidDismiss((data: string) => {
      if (data == 'confSuccess') {
        this.menuCtrl.enable(true);
        this.navCtrl.setRoot('HomePage');
      }
    });
    passwordModal.present();
  }

  signIn() {
    if (!this.loginForm.valid) {
      this.createToast('Please fill in ALL fields and correct any highlighted ones.', 3000, 'bottom');
    } else {
      let formData = this.loginForm.value;
      let loader = this.loadingCtrl.create({
        spinner: 'dots',
        content: 'Signing in...',
        dismissOnPageChange: true
      });
      loader.present().then(() => {
        this.apiService.signIn(formData).subscribe(userInfo => {
          let user = [];
          let accounts = [];
          if (userInfo['success'] === true) {
            user = userInfo['data'];
            accounts = userInfo['accounts'];
            let username = user['lastName'] + ' ' + user['firstName'] + ' ' + user['middleName'];
            this.storage.set('isLoggedIn', true);
            this.storage.set('userID', user['id']);
            this.storage.set('authToken', user['token']);
            this.storage.set('userNames', username).then(() => {
              this.events.publish('username:changed', username);
            });
            this.storage.set('userAvi', user['userAvatar']).then(() => {
              this.events.publish('avi:changed', user['userAvatar']);
            });
            this.storage.set('lastLogin', this.loginForm.get('phone').value).then(() => {
              for (let i = 0, len = accounts.length; i < len; i++) {
                this.accDBService.add(accounts[i]);
              }
              loader.dismiss().then(() => {
                this.menuCtrl.enable(true);
                this.navCtrl.setRoot('HomePage');
              });
            })
          } else {
            loader.dismiss().then(() => {
              this.createToast(userInfo['msg'], 3000, 'bottom', true);
            });
          }
        }, err => {
          loader.dismiss().then(() => {
            this.createToast('Oops! Error logging in. Please check your connectivity status and try again.', 4000, 'bottom');
          });
        });
      });
    }
  }

  passReset() {
    let prAlert = this.alertCtrl.create({
      title: 'Reset Password',
      inputs: [
        {
          name: 'email',
          placeholder: 'Enter your account email',
          type: 'email'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Reset',
          handler: data => {

            this.apiService.forgotPass({ email: data.email }).subscribe(res => {
              if (res['success'] === true) {
                this.createToast('Password Reset instructions have been sent to your email.', 3000, 'bottom');
              } else {
                this.createToast('Oops! There was a problem resetting your password.', 3000, 'bottom');
              }
            })
          }
        }
      ]
    });
    prAlert.present();
  }

  createToast(msg: string, dur: number, pos: string, btn: boolean = false) {
    this.toastCtrl.create({
      message: msg,
      duration: dur,
      position: pos,
      showCloseButton: btn
    }).present();
  }
}
