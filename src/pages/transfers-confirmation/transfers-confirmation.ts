import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams, LoadingController, ToastController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';

import { TransactionsNetServiceProvider } from '../../providers/transactions-net-service/transactions-net-service';
import { AccountsDbServiceProvider } from '../../providers/accounts-db-service/accounts-db-service';

@IonicPage()
@Component({
  selector: 'page-transfers-confirmation',
  templateUrl: 'transfers-confirmation.html',
})
export class TransfersConfirmationPage {
  cTransferForm: FormGroup;

  authToken: string = null;
  accID: number = null;

  constructor(private viewCtrl: ViewController, private params: NavParams, private formBuilder: FormBuilder, private storage: Storage, private loadingCtrl: LoadingController, private toastCtrl: ToastController, private alertCtrl: AlertController, private txnNetService: TransactionsNetServiceProvider, private accDBService: AccountsDbServiceProvider) {
    this.accID = this.params.get('accID');
    this.cTransferForm = this.formBuilder.group({
      accID: new FormControl({ disabled: false, value: this.accID }, Validators.compose([Validators.pattern('[1-9]*'), Validators.required])),
      accPIN: new FormControl({ disabled: false, value: '' }, Validators.compose([Validators.pattern('[a-zA-Z0-9]*'), Validators.required])),
      txnPIN: new FormControl({ disabled: false, value: '' }, Validators.compose([Validators.pattern('[a-zA-Z0-9]*'), Validators.required]))
    })
    this.storage.get('authToken').then(res => {
      if (typeof res != 'undefined' && res) {
        this.authToken = res;
      }
    });
  }

  ionViewDidLoad() {
  }

  finish() {
    if (!this.cTransferForm.valid) {
      this.createToast('Please fill in ALL fields and correct any highlighted ones.', 3000, 'bottom');
    } else {
      let formData = this.cTransferForm.value;
      let loader = this.loadingCtrl.create({
        spinner: 'dots',
        content: 'Completing Transaction...',
        dismissOnPageChange: true
      });
      loader.present().then(() => {
        this.txnNetService.completeTxn(this.authToken, formData).subscribe(res => {
          if (res['success'] === true) {
            loader.dismiss().then(() => {
              this.alertCtrl.create({
                title: 'Success',
                message: res['msg'],
                enableBackdropDismiss: false,
                buttons: [
                  {
                    text: 'OKAY',
                    handler: data => {
                      this.accDBService.findAccount(this.accID).then(acc => {
                        let account = acc['docs'][0];
                        account.AmountAvailable = parseFloat(res['data'].AmountAvailable).toFixed(2);
                        this.accDBService.update(account);
                        this.viewCtrl.dismiss();
                      })
                    }
                  }
                ]
              }).present();
            });
          } else {
            loader.dismiss().then(() => {
              this.createToast(res['msg'], 3000, 'bottom');
            });
          }
        }, err => {
          loader.dismiss().then(() => {
            this.createToast('Oops!! Error completing transaction. Please check your connectivity status and try again.', 4000, 'bottom');
          });
        });
      });
    }
  }

  cancel() {

  }

  createToast(msg: string, dur: number, pos: string, cls: boolean = false) {
    this.toastCtrl.create({
      message: msg,
      duration: dur,
      position: pos,
      showCloseButton: cls
    }).present();
  }
}
