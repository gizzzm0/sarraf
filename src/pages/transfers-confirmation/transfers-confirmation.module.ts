import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransfersConfirmationPage } from './transfers-confirmation';

@NgModule({
  declarations: [
    TransfersConfirmationPage,
  ],
  imports: [
    IonicPageModule.forChild(TransfersConfirmationPage),
  ],
})
export class TransfersConfirmationPageModule {}
