import { Component, NgZone } from '@angular/core';
import { IonicPage, Platform, ModalController, AlertController, ToastController, LoadingController, Loading } from 'ionic-angular';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';

import { AccountsDbServiceProvider } from '../../providers/accounts-db-service/accounts-db-service';
import { AccountsNetServiceProvider } from '../../providers/accounts-net-service/accounts-net-service';
import { TransactionsNetServiceProvider } from '../../providers/transactions-net-service/transactions-net-service';

@IonicPage()
@Component({
  selector: 'page-transfers',
  templateUrl: 'transfers.html',
})
export class TransfersPage {
  transferForm: FormGroup;
  loader: Loading;

  myAcc: number = null;
  accounts = [];
  authToken: string = null;
  canSend: boolean = false;
  availableAmt: number = 0;
  accCurrency: string = '';
  fxRate: number = null;
  recAmt: string = '0';
  sendC: string = '';
  receiveC: string = '';

  constructor(private platform: Platform, private dbService: AccountsDbServiceProvider, private zone: NgZone, private storage: Storage, private modalCtrl: ModalController, private alertCtrl: AlertController, private toastCtrl: ToastController, private loadingCtrl: LoadingController, private formBuilder: FormBuilder, private accountsNetService: AccountsNetServiceProvider, private txnNetService: TransactionsNetServiceProvider) {
    this.transferForm = this.formBuilder.group({
      SendAcc: new FormControl({ disabled: false, value: '' }, Validators.compose([Validators.pattern('[1-9]*'), Validators.required])),
      SendAmt: new FormControl({ disabled: true, value: '' }, Validators.compose([Validators.pattern('[0-9.]*'), Validators.required])),
      RecAccEmail: new FormControl({ disabled: true, value: '' }, Validators.compose([Validators.email, Validators.required])),
      RecAcc: new FormControl({ disabled: true, value: '' }, Validators.compose([Validators.pattern('[1-9]*'), Validators.required])),
      RecAccName: new FormControl({ disabled: true, value: '' }, Validators.required),
    });
    this.storage.get('authToken').then(res => {
      if (typeof res != 'undefined' && res) {
        this.authToken = res;
      }
    });
  }
  ionViewDidLoad() {
    this.platform.ready().then(() => {
      this.dbService.getAll().then(data => {
        let hasAccounts = Object.keys(data).length;
        if (hasAccounts !== 0) {
          this.zone.run(() => {
            this.accounts = data;
          });
        } else {
          this.createToast('Sorry! You have not configured any accounts yet.', 3000, 'bottom');
        }
      });
    });
  }

  sendMoney() {
    if (!this.transferForm.valid) {
      this.createToast('Please fill in ALL fields and correct any highlighted ones.', 3000, 'bottom');
    } else {
      let formData = this.transferForm.getRawValue();
      console.log(formData);
      this.createLoading('Processing transaction...');
      this.txnNetService.sendMoney(this.authToken, formData).subscribe(res => {
        if (res['success'] === true) {
          this.loader.dismiss().then(() => {
            let confirmModal = this.modalCtrl.create('TransfersConfirmationPage', { accID: this.transferForm.get('SendAcc').value });
            confirmModal.onDidDismiss(() => {
              this.recAmt = '0';
              this.fxRate = null;
              this.sendC = '';
              this.receiveC = '';
              this.transferForm.get('RecAcc').reset({ disabled: true, value: '' });
              this.transferForm.get('RecAccName').reset({ disabled: true, value: '' });
            });
            confirmModal.present();
          });
        } else {
          this.loader.dismiss().then(() => {
            this.createToast(res['msg'], 3000, 'bottom');
          });
        }
      }, err => {
        this.loader.dismiss().then(() => {
          this.createToast('Oops!! Error processing transaction. Please check your connectivity status and try again or contact Surraf customer support.', 4000, 'bottom', true);
        });
      });
    }
  }

  getFxRate() {
    let sending = this.transferForm.get('SendAcc').value;
    let receiving = this.transferForm.get('RecAcc').value;
    if (typeof (sending) != 'undefined' && typeof (receiving) != 'undefined' && sending > 0 && receiving > 0) {
      this.createLoading('Getting forex exchange rate...');
      this.txnNetService.getFX(this.authToken, sending, receiving).subscribe(res => {
        if (res['success'] === true) {
          this.loader.dismiss().then(() => {
            this.fxRate = res['data'].rate;
            this.sendC = res['data'].send;
            this.receiveC = res['data'].receive;
            this.recAmt = (parseFloat(this.transferForm.get('SendAmt').value) / this.fxRate).toFixed(2);
          });
        } else {
          this.loader.dismiss().then(() => {
            this.createToast(res['msg'], 3000, 'bottom');
          });
        }
      }, err => {
        this.loader.dismiss().then(() => {
          this.createToast('Oops!! Error getting rate. Please check your connectivity status and try again.', 4000, 'bottom');
        });
      });
    } else {
      this.createToast('Please provide the correct account details', 2000, 'bottom');
    }
  }

  getAccounts() {
    let email = this.transferForm.get('RecAccEmail').value;
    if (typeof (email) !== 'undefined' && email && this.validateEmail(email)) {
      this.createLoading('Getting user\'s accounts...');
      this.accountsNetService.getUserAccounts(this.authToken, email).subscribe(res => {
        if (res['success'] === true) {
          this.loader.dismiss().then(() => {
            let accModal = this.modalCtrl.create('AccountSearchPage', { accounts: res['data'] });
            accModal.onDidDismiss(data => {
              if (typeof data !== 'undefined' && data) {
                this.transferForm.get('RecAcc').reset({ disabled: true, value: data.aID });
                this.transferForm.get('RecAccName').reset({ disabled: true, value: data.aName });
                this.getFxRate();
              } else {
                this.transferForm.get('RecAcc').reset({ disabled: true, value: '' });
                this.transferForm.get('RecAccName').reset({ disabled: true, value: '' });
              }
            });
            accModal.present();
          });
        } else {
          this.loader.dismiss().then(() => {
            this.createToast(res['msg'], 3000, 'bottom', true);
          });
        }
      }, err => {
        this.loader.dismiss().then(() => {
          console.log(err);
          this.createToast('Oops!! Error getting accounts. Please check your connectivity status and try again.', 4000, 'bottom');
        });
      });
    } else {
      this.createToast('Please enter a valid email.', 2000, 'bottom');
    }
  }

  checkAmt() {
    let amtSet = this.transferForm.get('SendAmt').value;
    if (parseFloat(amtSet) > this.availableAmt) {
      this.alertCtrl.create({
        title: 'Oops!',
        subTitle: 'Insufficient Balance',
        message: 'The transaction amount CANNOT exceed the available amount. Please top up and try again.',
        buttons: ['OK. Got it.']
      }).present();
      this.transferForm.get('RecAccEmail').reset({ disabled: true, value: '' });
      this.transferForm.get('RecAcc').reset({ disabled: true, value: '' });
      this.transferForm.get('RecAccName').reset({ disabled: true, value: '' });
    } else {
      this.transferForm.get('RecAccEmail').reset({ disabled: false, value: '' });
      this.transferForm.get('RecAcc').reset({ disabled: true, value: '' });
      this.transferForm.get('RecAccName').reset({ disabled: true, value: '' });
    }
  }

  activateAmount(id) {
    if (id !== null && id > 0) {
      this.createLoading('Confirming available balance...');
      this.accountsNetService.getAvailableBal(this.authToken, id).subscribe(res => {
        if (res['success'] === true) {
          this.loader.dismiss().then(() => {
            if (parseFloat(res['data'].accBalance) > 0) {
              this.availableAmt = parseFloat(res['data'].accBalance);
              this.accCurrency = res['data'].accCurr;
              this.transferForm.get('SendAmt').reset({ disabled: false, value: '0.00' });
              this.alertCtrl.create({
                title: 'Available Balance',
                subTitle: this.accCurrency + ' ' + this.formatCurrency(this.availableAmt.toFixed(2)),
                buttons: ['OK. Got it.']
              }).present();
            } else {
              this.createToast('Sorry! You do not have enough balance to transact. Please top up your account.', 3000, 'bottom');
              this.transferForm.get('SendAmt').reset({ disabled: true, value: '' });
              this.availableAmt = 0;
              this.accCurrency = '';
            }
          });
        } else {
          this.loader.dismiss().then(() => {
            this.createToast(res['msg'], 3000, 'bottom');
          });
        }
      }, err => {
        this.loader.dismiss().then(() => {
          console.log(err);
          this.createToast('Oops!! Error getting balance. Please check your connectivity status and try again.', 4000, 'bottom');
        });
      });
    } else {
      this.createToast('Please enter a valid account.', 2000, 'bottom');
    }
  }

  createToast(msg: string, dur: number, pos: string, cls: boolean = false) {
    this.toastCtrl.create({
      message: msg,
      duration: dur,
      position: pos,
      showCloseButton: cls
    }).present();
  }

  createLoading(msg: string) {
    this.loader = this.loadingCtrl.create({
      spinner: 'dots',
      content: msg,
      dismissOnPageChange: true
    });
    this.loader.present();
  }

  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  formatCurrency(amt) {
    return amt.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
}
