import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-transactions',
  templateUrl: 'transactions.html',
})
export class TransactionsPage {

  constructor(public navCtrl: NavController) {
  }

  navigate(page: string) {
    if (typeof (page) !== 'undefined' && page) {
      this.navCtrl.push(page);
    }
  }
}
