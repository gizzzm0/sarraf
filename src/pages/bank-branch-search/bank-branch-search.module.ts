import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BankBranchSearchPage } from './bank-branch-search';

@NgModule({
  declarations: [
    BankBranchSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(BankBranchSearchPage),
  ]
})
export class BankBranchSearchPageModule { }
