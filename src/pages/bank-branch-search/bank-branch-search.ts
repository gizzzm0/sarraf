import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavParams, ViewController, Searchbar } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-bank-branch-search',
  templateUrl: 'bank-branch-search.html',
})
export class BankBranchSearchPage {
  @ViewChild('mainSearchbar') searchBar: Searchbar;
  bankBranchesRaw = [];
  bankBranchesFiltered = [];

  constructor(private viewCtrl: ViewController, private params: NavParams) {
    this.bankBranchesRaw = this.params.get('bank_branches_list');
  }

  ionViewDidEnter() {
    setTimeout(() => {
      this.searchBar.setFocus();
      this.searchBar.placeholder = 'Search branch name...';
      this.searchBar.autocorrect = 'on';
    }, 100);
  }

  selectBankBranch(branchID, branchName) {
    let res = { 'brID': branchID, 'brName': branchName };
    this.viewCtrl.dismiss(res);
  }

  filterBankBranches(ev: any) {
    this.bankBranchesFiltered = [];
    let val = ev.target.value;

    if (val && val.trim() != '') {
      this.bankBranchesRaw.filter(branch => {
        if (branch["branchName"].match(new RegExp(val, "i"))) {
          this.bankBranchesFiltered.push({
            branchid: branch["branchID"],
            branchname: branch["branchName"]
          });
        }
      });
    } else {
      this.bankBranchesFiltered = [];
    }
  }

}
