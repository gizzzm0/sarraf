import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavParams, ViewController, Searchbar } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-account-search',
  templateUrl: 'account-search.html',
})
export class AccountSearchPage {
  @ViewChild('mainSearchbar') searchBar: Searchbar;
  accountsRaw = [];
  accountsFiltered = [];

  constructor(private viewCtrl: ViewController, private params: NavParams) {
    this.accountsRaw = this.params.get('accounts');
    this.accountsFiltered = this.params.get('accounts');
  }

  ionViewDidEnter() {
    setTimeout(() => {
      this.searchBar.setFocus();
      this.searchBar.placeholder = 'Search acc name...';
      this.searchBar.autocorrect = 'on';
    }, 100);
  }

  selectAccount(accID, accName, accCurr) {
    let res = { 'aID': accID, 'aName': accName + ' (' + accCurr + ')' };
    this.viewCtrl.dismiss(res);
  }

  filterAccounts(ev: any) {
    this.accountsFiltered = [];
    let val = ev.target.value;

    if (val && val.trim() != '') {
      this.accountsRaw.filter(account => {
        if (account["accountName"].match(new RegExp(val, "i"))) {
          this.accountsFiltered.push({
            accountID: account["accountID"],
            accountCurrencyID: account["accountCurrencyID"],
            accountName: account["accountName"]
          });
        }
      });
    } else {
      this.accountsFiltered = this.accountsRaw;
    }
  }
}
