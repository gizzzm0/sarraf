import { NgModule } from '@angular/core';
import { NoDataComponent } from './no-data/no-data';

@NgModule({
    declarations: [NoDataComponent
    ],
    entryComponents: [
    ],
    imports: [
    ],
    exports: [NoDataComponent
    ],
    schemas: [
    ]
})
export class ComponentsModule { }
