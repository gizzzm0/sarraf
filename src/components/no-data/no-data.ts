import { Component } from '@angular/core';

@Component({
  selector: 'no-data',
  templateUrl: 'no-data.html'
})
export class NoDataComponent {

  text: string;

  constructor() {
    console.log('Hello NoDataComponent Component');
    this.text = 'Hello World';
  }

}
